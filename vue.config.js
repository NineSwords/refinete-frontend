module.exports = {
  lintOnSave: false,

  pwa: {
    name: 'refinete',
    themeColor: '#46BAF9',
    msTileColor: '#0377FE'
  }
}